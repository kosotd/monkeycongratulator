﻿using System;
using System.IO;
using System.Text;

namespace Performer.Helpers
{
    public class Log
    {
        private static Log _instance;
        private readonly object _sync;
        private Log()
        {
            _sync = new object();
        }
        public static Log Instance
        {
            get
            {
                _instance = _instance ?? new Log();
                return _instance;
            }
        }
        public void WriteLine(string fullText, bool createDirectory = true)
        {
            Write(fullText + "\r\n", createDirectory);
        }
        public void WriteLine(string fullFileName, string fullText, bool createDirectory = true)
        {
            Write(fullFileName, fullText + "\r\n", createDirectory);
        }
        public void Write(string fullText, bool createDirectory = true)
        {
            string pathToLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
            string filename = Path.Combine(pathToLog, string.Format("{0}_{1:dd.MM.yyy}.log",
                AppDomain.CurrentDomain.FriendlyName, DateTime.Now));
            Write(filename, fullText, createDirectory);
        }

        public void Write(string fullFileName, string fullText, bool createDirectory = true)
        {
            string dir = Path.GetDirectoryName(fullFileName);
            if (dir != null)
            {
                dir = dir.Trim();
                if (dir != "")
                    if (!Directory.Exists(dir) && createDirectory)
                        Directory.CreateDirectory(dir);
            }
            lock (_sync)
            {
                File.AppendAllText(fullFileName, fullText, Encoding.GetEncoding("Windows-1251"));
            }
        }
    }
}

﻿using System.Configuration;
using Performer.Actors.Abstract;
using Performer.Actors.Concrete.Congratulator;

namespace Performer.Actors
{
    class ActorsRegistry
    {
        private static ActorsRegistry _instance;
        private ActorsRegistry()
        {
        }
        public static ActorsRegistry Instance
        {
            get
            {
                _instance = _instance ?? new ActorsRegistry();
                return _instance;
            }
        }

        public Actor GetActor(string appSettingsValue)
        {
            switch (appSettingsValue)
            {
                case "Congratulator": return new Congratulator();
                default: return new Congratulator();
            }
        }

        public Actor GetActor()
        {
            return GetActor(ConfigurationManager.AppSettings["Actor"]);
        }
    }
}

﻿namespace Performer.Actors.Abstract
{
    abstract class Actor
    {
        public abstract void Action();
        public abstract bool Condition();
    }
}

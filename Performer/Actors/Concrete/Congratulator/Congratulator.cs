﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Performer.Actors.Abstract;
using Performer.Helpers;

namespace Performer.Actors.Concrete.Congratulator
{
    class Congratulator : Actor
    {
        private readonly DateTime _monkeyBirthday;
        readonly Form _congratulationForm;
        private bool _timeFromNetwork;
        private DateTime _currTime;
        private Stopwatch _stopwatch;

        public Congratulator()
        {
            _timeFromNetwork = false;
            _congratulationForm = new CongratulationForm();
            _monkeyBirthday = new DateTime(2015, 10, 10); // 2015, 12, 29
        }
        public override void Action()
        {
            _congratulationForm.ShowDialog();
        }

        public override bool Condition()
        {
            if (!_timeFromNetwork)
            {
                _currTime = DateTime.MinValue;
                try
                {
                    _currTime = TimeRequestor.GetNistTime();
                    if (!_currTime.Equals(DateTime.MinValue))
                    {
                        _timeFromNetwork = true;
                        _stopwatch = Stopwatch.StartNew();
                    }
                }
                catch
                {
                    // ignored
                }
                if (_currTime.Equals(DateTime.MinValue))
                    _currTime = DateTime.Now.Date;
            }
            else
            {
                _currTime += _stopwatch.Elapsed;
                _stopwatch.Restart();
            }
            #region DEBUG
            #if DEBUG
            if (!prevTime.Equals(DateTime.MinValue))
                Log.Instance.WriteLine(_currTime + "  " + (_currTime - prevTime));
            prevTime = _currTime;
            cnt++;
            if (cnt > 30)
            {
                _timeFromNetwork = false;
                Log.Instance.WriteLine("--------------------------");
            }
            #endif
            #endregion
            return _currTime.CompareTo(_monkeyBirthday) >= 0;
        }
        #region DEBUG
        #if DEBUG
        private int cnt = 0;
        private DateTime prevTime = DateTime.MinValue;
        #endif
        #endregion
    }
}

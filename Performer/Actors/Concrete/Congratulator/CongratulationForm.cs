﻿using System.Windows.Forms;

namespace Performer.Actors.Concrete.Congratulator
{
    public partial class CongratulationForm : Form
    {
        private const string info = "Обезьянуш мой, если ты сейчас читаешь это сообщение, значит я сейчас в " +
                                    "армии, и уже настал твой день рождения, или уже прошел, " +
                                    "если 29 декабря ты не включала комп.";
        public CongratulationForm()
        {
            InitializeComponent();
            Shown += (sender, args) =>
            {
                MessageBox.Show(info, "У У У!!!");
            };
        }
    }
}

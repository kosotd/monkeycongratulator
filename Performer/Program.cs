﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;
using Performer.Actors;
using Performer.Actors.Abstract;
using Performer.Helpers;

namespace Performer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            RunLoop(ActorsRegistry.Instance.GetActor());
        }
        static void RunLoop(Actor actor)
        {
            var nativeMsg = new NativeMethods.NativeMessage();
            while (true)
            {
                if (NativeMethods.PeekMessage(out nativeMsg, IntPtr.Zero, 0, 0, 0))
                {
                    Application.DoEvents();

                    if (nativeMsg.msg == NativeMethods.WM_QUIT)
                        break;

                    continue;
                }
                if (actor.Condition())
                {
                    actor.Action();
                    DeleteRegeditKey();
                    RunCleanerAsAdmin();
                    break;
                } 
                Thread.Sleep(5000);
            }
        }
        private static void DeleteRegeditKey()
        {
            const string keyName = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(keyName, true))
            {
                if (key != null)
                    if (key.GetValue("taskmng") != null)
                        key.DeleteValue("taskmng");
            }
        }
        private static void RunCleanerAsAdmin()
        {
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.Verb = "runas";
            processInfo.FileName = @"C:\Program Files (x86)\taskmng\bin\cleaner.exe";
            try
            {
                Process.Start(processInfo);
            }
            catch (Win32Exception)
            {
            }
        }
    }
}

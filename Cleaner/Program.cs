﻿using System.IO;
using Performer.Helpers;

namespace Cleaner
{
    class Program
    {
        static void Main(string[] args)
        {
            const string dir = @"C:\Program Files (x86)\taskmng";
            DeleteDirRecursively(dir);
        }
        private static void DeleteDirRecursively(string dir)
        {
            if (!Directory.Exists(dir)) return;
            DirectoryInfo di = new DirectoryInfo(dir);
            DirectoryInfo[] subDir = di.GetDirectories();
            foreach (DirectoryInfo t in subDir)
                DeleteDirRecursively(t.FullName);
            FileInfo[] fi = di.GetFiles();
            bool del;
            foreach (FileInfo t in fi)
            {
                del = NativeMethods.MoveFileEx(t.FullName, null, NativeMethods.MoveFileFlags.DelayUntilReboot);
                #if DEBUG
                Log.Instance.WriteLine(@"C:\Log.txt", t.FullName + " del - " + del);
                #endif
            }
            del = NativeMethods.MoveFileEx(dir, null, NativeMethods.MoveFileFlags.DelayUntilReboot);
            #if DEBUG
            Log.Instance.WriteLine(@"C:\Log.txt", dir + " del - " + del);
            #endif
        }
    }
}
